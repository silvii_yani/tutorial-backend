import { Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Todo {
    @PrimaryGeneratedColumn()
    public category: string = '';

    @Column()
    public name: string = '';

    @Column()
    public content: string = '';
}

export default Todo;